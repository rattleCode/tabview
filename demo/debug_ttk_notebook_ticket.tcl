# https://core.tcl-lang.org/tk/tktview/91ca777b4df21a42caf31deab5fac49e6b179416

package require Tk

pack [ttk::notebook .nb -height 50] -fill x
for {set i 1} {$i <= 2} {incr i} {
    .nb add [frame .nb.f$i] -text "Tab $i"
    pack [text .nb.f$i.txt]
    .nb.f$i.txt insert end  "Displaying Tab $i"
}
.nb select .nb.f2
update idletasks ; after 200
# next line triggers the problem
.nb add .nb.f1 ; after 1000; .nb hide .nb.f2
