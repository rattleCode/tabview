 ########################################################################
 #
 # demo.tcl
 # by Manfred ROSENBERGER
 #
 #   (c) Manfred ROSENBERGER 2022/07/13
 #
 #

set TEST_ROOT_Dir [file normalize [file dirname [lindex $argv0]]]
    #
set TEST_Library_Dir [file dirname [file dirname $TEST_ROOT_Dir]]
    #
puts "    -> \$TEST_ROOT_Dir ...... $TEST_ROOT_Dir"
puts "    -> \$TEST_Library_Dir ... $TEST_Library_Dir"
    #
    #
lappend auto_path [file dirname $TEST_ROOT_Dir]
lappend auto_path "$TEST_Library_Dir"
lappend auto_path [file join $TEST_Library_Dir __ext_Libraries]
    #
foreach dir $tcl_library {
    puts "   -> tcl_library $dir"
}    
    #
foreach dir $auto_path {
    puts "   -> auto_path   $dir"
}    
    #
    #
    # ------------------
    #
package require tabControl 0.03
catch {
    package require dicttool
}
catch {
    package require ttkThemes
    ttk::setTheme clearlogic;   # clearlogic black Classic
}
    #
    #
variable objTabControl  
    #
wm minsize . 700 400
    #
frame   .f      -width 560  -height 540 -bg lightgreen
    #
frame   .f.f1   -width 560  -height  50 -bg darkgray
frame   .f.f2_1 -width 160  -height 350 -bg blue
frame   .f.f2_2 -width 260  -height 200 -bg lightblue   ;# contains ttk::notebook
frame   .f.f2_3 -width 260  -height  50 -bg red         ;# contains tabControl
frame   .f.f4   -width 260  -height  40 -bg green
    #
# create_TabControl .f2_2
    #
grid columnconfigure .  0 -weight 1
grid rowconfigure    .  0 -weight 1
    #
grid .f                 -sticky nsew
grid rowconfigure    .f 0 -weight 0
grid rowconfigure    .f 1 -weight 1
grid rowconfigure    .f 2 -weight 0
    #
grid columnconfigure .f 0 -weight 0
grid columnconfigure .f 1 -weight 1
    #
    #
grid .f.f1              -row 0 -columnspan 2
grid .f.f2_1 .f.f2_2
grid .f.f2_3            -column 1
grid .f.f4              -columnspan 2
    #
grid configure .f.f1    -sticky ew
grid configure .f.f2_1  -sticky ns  -rowspan 2
grid configure .f.f2_2  -sticky nsew
grid configure .f.f2_3  -sticky ew
grid configure .f.f4    -sticky ew
    #
grid columnconfigure .f.f2_3 0 -weight 1
    #
    #
    # -- ttk::notebook
    #
set nb  [ttk::notebook .f.f2_2.nb  -width 100 -height 100]  ;#  -style Tabless.TNotebook
pack $nb -fill both -expand yes
    #
$nb add [ttk::frame $nb.f0] -text "TabNull" 
$nb add [ttk::frame $nb.f1] -text "TabOne" 
$nb add [ttk::frame $nb.f2] -text "TabTwo"
$nb add [ttk::frame $nb.f3] -text "TabThree"
$nb add [ttk::frame $nb.f4] -text "TabFour"
$nb add [ttk::frame $nb.f5] -text "V__V"
$nb add [ttk::frame $nb.f6] -text "A__A"
    #
pack [label $nb.f0.bt -background green -foreground white -text "TabNull"]
pack [label $nb.f1.bt -background red   -foreground white -text "TabOne"]
pack [label $nb.f2.bt -background red   -foreground white -text "TabTwo"]
pack [label $nb.f3.bt -background blue  -foreground white -text "TabThree"]
pack [label $nb.f4.bt -background blue  -foreground white -text "TabFour"]    
pack [label $nb.f5.bt -background blue  -foreground white -text "V__V"]    
pack [label $nb.f6.bt -background blue  -foreground white -text "A__A"]    
    #
$nb hide $nb.f3
    #
    #
    # -- tabControl::TabControl
    #
set objTabControl   [tabControl::TabControl new .f.f2_3._tC -offsetLeft 30 -paddingText 10]
set ctrlView        [$objTabControl getPath]
    # puts "  <D> $ctrlView"
grid $ctrlView      -sticky ew
    #
    #
    # -- occupy ttk::notebook
    #
$objTabControl occupyWidget $nb
    #
    #
    # -- build some test options
    #
button .f.f2_1.bt_r -text "report" -command [list reportRegistry]
pack .f.f2_1.bt_r -side top -fill x 
    #
proc reportRegistry {} {
    variable    objTabControl
    puts "\n ================================\n"
    catch {puts "[dict print [$objTabControl getRegistry]]"}
    puts "\n --------------------------------\n"
        #
    puts "    ... current: <-?-> [$objTabControl select]"
}
    #
button .f.f2_1.bt_h1 -text "hide: 0" -command [list commandHandler hide "0"]
pack .f.f2_1.bt_h1 -side top -fill x 
button .f.f2_1.bt_h2 -text "hide: 1" -command [list commandHandler hide "1"]
pack .f.f2_1.bt_h2 -side top -fill x
    #
button .f.f2_1.bt_f1 -text "forget: 0" -command [list commandHandler forget "0"]
pack .f.f2_1.bt_f1 -side top -fill x 
button .f.f2_1.bt_f2 -text "forget: 1" -command [list commandHandler forget "1"]
pack .f.f2_1.bt_f2 -side top -fill x 
    #
button .f.f2_1.bt_a1 -text "add: 0" -command [list commandHandler add "0"]
pack .f.f2_1.bt_a1 -side top -fill x 
button .f.f2_1.bt_a2 -text "add: 1" -command [list commandHandler add "1"]
pack .f.f2_1.bt_a2 -side top -fill x
    #
button .f.f2_1.bt_hn1 -text "hide: TabOne" -command [list commandHandler hideByName "TabOne"]
pack .f.f2_1.bt_hn1 -side top -fill x 
button .f.f2_1.bt_hn2 -text "hide: TabWo" -command [list commandHandler hideByName "TabWo"]
pack .f.f2_1.bt_hn2 -side top -fill x 
    #
button .f.f2_1.bt_fn1 -text "forget: TabOne" -command [list commandHandler forgetByName "TabOne"]
pack .f.f2_1.bt_fn1 -side top -fill x 
button .f.f2_1.bt_fn2 -text "forget: TabWo" -command [list commandHandler forgetByName "TabWo"]
pack .f.f2_1.bt_fn2 -side top -fill x 
    #
button .f.f2_1.bt_an1 -text "add: TabOne" -command [list commandHandler addByName "TabOne"]
pack .f.f2_1.bt_an1 -side top -fill x 
button .f.f2_1.bt_an2 -text "add: TabWo" -command [list commandHandler addByName "TabTwo"]
pack .f.f2_1.bt_an2 -side top -fill x 
    #
    #
button .f.f2_1.bt_at1 -text "newTab: end" -command [list commandHandler addNewTab "end"]
pack .f.f2_1.bt_at1 -side top -fill x 
    #
button .f.f2_1.bt_st1 -text "theme: black" -command [list commandHandler switchTheme "black"]
pack .f.f2_1.bt_st1 -side top -fill x 
    #
button .f.f2_1.bt_st2 -text "theme: clearlogic" -command [list commandHandler switchTheme "clearlogic"]
pack .f.f2_1.bt_st2 -side top -fill x 
    #
button .f.f2_1.bt_st3 -text "theme: kroc" -command [list commandHandler switchTheme "kroc"]
pack .f.f2_1.bt_st3 -side top -fill x 
    #
button .f.f2_1.bt_st4 -text "theme: BlueElegance" -command [list commandHandler switchTheme "BlueElegance"]
pack .f.f2_1.bt_st4 -side top -fill x 
    #
button .f.f2_1.bt_st5 -text "theme: plastik" -command [list commandHandler switchTheme "plastik"]
pack .f.f2_1.bt_st5 -side top -fill x 
    #
button .f.f2_1.bt_st6 -text "theme: aquablue" -command [list commandHandler switchTheme "aquablue"]
pack .f.f2_1.bt_st6 -side top -fill x 
    #
    #
button .f.f2_1.bt_sC1 -text "switchNativeControl" -command [list commandHandler switchNativeNotebookControl]
pack .f.f2_1.bt_sC1 -side top -fill x 
    #
button .f.f2_1.bt_cS1 -text "manageTabs - 001" -command [list commandHandler manageTabs "001"]
pack .f.f2_1.bt_cS1 -side top -fill x 
button .f.f2_1.bt_cS2 -text "manageTabs - 002" -command [list commandHandler manageTabs "002"]
pack .f.f2_1.bt_cS2 -side top -fill x 
    #
    #
proc commandHandler {command args} {
    variable objTabControl
    upvar nb myNb
        #
    puts "    -> commandHandler: $command $args"
        #
    switch -exact -- $command {
        add {
            $objTabControl add $args
        }
        forget {
            $objTabControl forget $args
        }
        hide {
            $objTabControl hide $args
        }
        addByName {
            $objTabControl addByName $args
        }
        forgetByName {
            $objTabControl forgetByName $args
        }
        hideByName {
            $objTabControl hideByName $args
        }
        addNewTab {
            set text    [clock format [clock seconds] -format "%H:%M:%S"]
            set frame   [ttk::frame $myNb.f_[clock format [clock seconds] -format "%H%M%S"]]
            pack [label $frame.bt -background blue -foreground white -text $text]
            $objTabControl add $frame -text $text
            after 1000  ;# for secure unique widgets 
        }
        switchTheme {
            catch {
                ttk::setTheme $args
                $objTabControl updateTabView
            }
        }
        manageTabs {
                #
            puts "-> \$objTabControl:"
            set currentState [$objTabControl switchTabsState]
            if {[catch {puts "[dict print $currentState]"} eID]} {
                puts $currentState
            }
            puts "-> \$myNb: [$myNb select]"
            foreach tab [$myNb tabs] {
                puts "      -> $tab"
                puts "        -text [$myNb tab $tab -text]"
                puts "        -state [$myNb tab $tab -state]"
            }
                #
            if {![winfo exists $myNb.f_1]} { $objTabControl add [ttk::frame $myNb.f_1] -text "T_One"  }
            if {![winfo exists $myNb.f_2]} { $objTabControl add [ttk::frame $myNb.f_2] -text "T_Two"  }
            if {![winfo exists $myNb.f_3]} { $objTabControl add [ttk::frame $myNb.f_3] -text "T_Three"}
            if {![winfo exists $myNb.f_4]} { $objTabControl add [ttk::frame $myNb.f_4] -text "T_Four" }
            if {![winfo exists $myNb.f_5]} { $objTabControl add [ttk::frame $myNb.f_5] -text "T_Five" }
             # {![winfo exists $myNb.f_6]} { $objTabControl add [ttk::frame $myNb.f_6] -text "Tab_D_Six"  }
             # {![winfo exists $myNb.f_7]} { $objTabControl add [ttk::frame $myNb.f_7] -text "Tab_D_Seven"}
             # {![winfo exists $myNb.f_8]} { $objTabControl add [ttk::frame $myNb.f_8] -text "Tab_D_Eight"}
             # {![winfo exists $myNb.f_9]} { $objTabControl add [ttk::frame $myNb.f_9] -text "Tab_D_Nime" }
                #
            $objTabControl select $myNb.f_2
                #
            set currentState [$objTabControl switchTabsState]
            set currentTab   [$objTabControl select]
            puts "        -> \$currentTab $currentTab"
            if {[catch {puts "[dict print $currentState]"} eID]} {
                puts $currentState
            }
                #
            set previousState   $currentState    
                #
            set manageState     $currentState
                #
            switch -exact [lindex [join $args] 0] {
                001 {
                    dict set manageState  $myNb.f_1 -state normal
                    dict set manageState  $myNb.f_2 -state normal
                    dict set manageState  $myNb.f_3 -state hidden
                    dict set manageState  $myNb.f_4 -state hidden
                    dict set manageState  $myNb.f_5 -state hidden
                }
                002 {
                    dict set manageState  $myNb.f_1 -state selected
                    dict set manageState  $myNb.f_2 -state hidden
                    dict set manageState  $myNb.f_3 -state normal
                    dict set manageState  $myNb.f_4 -state hidden
                    dict set manageState  $myNb.f_5 -state normal
                }
            }
                #
            if {[catch {puts "[dict print $manageState]"} eID]} {
                puts $manageState
            }
                #
            $objTabControl switchTabsState $manageState
                #
            
        }
        switchNativeNotebookControl {
            $objTabControl switchRemoteControllerTabs
        }
    }
}
proc hideNoteBookTabs {} {
    upvar nb 1
}
