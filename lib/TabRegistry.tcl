 ########################################################################
 #
 #  Copyright (c) Manfred ROSENBERGER, 2022
 #
 # ----------------------------------------------------------------------
 #  namespace:  tabControl
 # ----------------------------------------------------------------------
 #
 #  0.00 - 2022/07/13
 #      ... 
 #
 
    #
oo::class create tabControl::Registry {
        #
    superclass tabControl::Subject
        #
    variable DictRegistry
    variable CurrentTabID
        #
    constructor {} {
            #
        next
            #
        set CurrentTabID    {}
            #
        set DictRegistry    {}
            #
    }
    
    destructor {
            # clean up once the window get's destroyed
        catch {[self] destroy}
            #
    }
    
    method unknown {method args} {
        return -code error "method \"$method\" is unknown"
    }
    
        #
    method setRegistry {dict id_active} {
            # puts "tabControl::Registry -> setRegistry"
            # puts $dict
        set DictRegistry $dict
        my select $id_active
    }
        #
    method add {window {name {}}} {
            # Adds a new tab to the tabControl. If window is currently managed by 
            # the notebook but hidden, it is restored to its previous position. 
            # puts "        ... tabControl::Registry -> add: $name -> $window"
            #
        set tabid   [my UnifyTabID $window]
            # puts "    -- add --> $tabid --"                
            #
            
        if {$tabid != {}} {
            # -- window allready known
            dict set DictRegistry   $tabid  -state normal
            my SetTabID_Current $tabid
            my notify {addTab}
        } else {
            dict set DictRegistry   $window [list -text $name -state normal]
            my SetTabID_Current $window
            my notify {addTabNew}
        }
            #
            # puts "    -- add --> $DictRegistry"
            #
        return 1
            #
    }
    method forget {tabid} {
            # puts "    -- forget --> $tabid --"
            # puts "[dict keys $DictRegistry]"
        set tabid   [my UnifyTabID $tabid]
        if {$tabid != {}} {
            set DictRegistry    [dict remove $DictRegistry $tabid]
            my CheckForCurrentTabID $tabid
            puts "[dict keys $DictRegistry]"
            my notify "forgetTab $tabid"
        }
    }
    method select {tabid} {
        set tabid   [my UnifyTabID $tabid]
        if {$tabid != {}} {
            my SetTabID_Current $tabid
            my notify {selectTab}
        } else {
            puts "        <I> tabControl::Registry -> select: Invalid slave specification: >$tabid<"
            # error "tabControl::Registry -> select: Invalid slave specification: $tabid"
        }
    }
    method hide {tabid} {
        set tabid   [my UnifyTabID $tabid]
        if {$tabid != {}} {
            dict set DictRegistry $tabid -state hidden    
            my CheckForCurrentTabID $tabid
            my notify {hideTab}
        }
    }
    method index {tabid} {
        return [lsearch -exact [dict keys $DictRegistry] $tabid]
    }    
    method tab {tabid args} {
            #
        set tabDict [dict get $DictRegistry $tabid]
            #
        if {$tabDict == {}} {
            error "tabControl::Registry -> tab: Invalid slave specification $tabid"
        }
            # puts "     -> $tabDict"
            # puts "     -> $args <- [llength [join $args]]"
            #
        if {[llength [join $args]] == 0} {
            return [list "-text" [dict get $tabDict -text]]
        } 
        if {[llength [join $args]] == 1} {
            if {$args eq "-text"} {
                return [dict get $tabDict -text]
            }
        } 
        if {[llength [join $args]] > 1} {
            set checkUpdate    0
            foreach {arg value} [join $args] {
                puts "          ... $arg -> $value"
                switch -exact -- $arg {
                    -text {
                       dict set DictRegistry $tabid -text $value
                       set checkUpdate 1
                    }
                    default {
                    }
                }
            }
                # puts "[dict get $DictRegistry $tabid]"
            if $checkUpdate {
                my notify {modifyTab}
            }
        }
            #
        return {}
            #
    }    
    method tabs {} {
        return [my GetTabIDs_All]
    }    
        #
    method switchTabsState {{dict {}}} {
        if {$dict == {}} {
            return $DictRegistry
        }
        foreach key [dict keys $dict] {
            if [dict exists $DictRegistry $key] {
                set newState    [dict get $dict $key -state]
                if {$newState eq "selected" || $newState eq "active"} {
                    set CurrentTabID $key
                }
                dict set DictRegistry $key -state [dict get $dict $key -state]
            }
        }
        my CheckForCurrentTabID
        my notify {modifyTabs}
            #
        return $DictRegistry
            #
    }
        #
    method addByName {name} {
        set tabid   [my GetTabID_Name $name]
        puts "    -- addByName --> $name --> $tabid"
        if {$tabid != {}} {
            my add $tabid $name
        }
    }
    method selectByName {name} {
        set tabid   [my GetTabID_Name $name]
        puts "    -- selectByName --> $name --> $tabid"
        if {$tabid != {}} {
            my select $tabid
        }
    }
    method forgetByName {name} {
        set tabid [my GetTabID_Name $name]
        puts "    -- forgetByName --> $name --> $tabid"
        if {$tabid != {}} {
            my forget $tabid
        }
    }
    method hideByName {name} {
        set tabid [my GetTabID_Name $name]
        puts "    -- hideByName --> $name --> $tabid"
        if {$tabid != {}} {
            my hide $tabid
        }
    }
        #
    method getCurrent_TabID {} {
        return $CurrentTabID
    }
    method getCurrent_Name {} {
        return [dict get $DictRegistry $CurrentTabID -text]
    }
    method getTabNames_All {} {
        return [my GetTabNames_All]
    }
    method getMap_AllNames {} {
            # return a map from all tabs with tabnames as key:
            #     name -> tabid
        set map {}
        dict for {w wDict} $DictRegistry {
            lappend map [dict get $wDict -text] $w 
        }
        return $map
    }
    method getMap_VisibleTabIDs {} {
            # return a map of window and tabname of all active tabs (active & normal):
            #     window -> name 
        set map {}
        dict for {w wDict} $DictRegistry {
            if {[dict get $wDict -state] ne "hidden"} {
                lappend map $w [dict get $wDict -text] 
            }                
        }
        return $map
    }

    method getCurrent_TabNeighbors {} {
        set neighbors {}
        set tabids  [my GetTabIDs_Visible]
        set currIndex [lsearch -exact $tabids $CurrentTabID]
            # puts "      -> \$currIndex $currIndex"
        if {$currIndex == 0} {
            lappend neighbors {}
        } else {
            lappend neighbors [lindex $tabids $currIndex-1]
        }
        if {$currIndex == [llength $tabids]-1} {
            lappend neighbors {}
        } else {
            lappend neighbors [lindex $tabids $currIndex+1]
        }
            # puts "      -> \$neighbors $neighbors"
        return $neighbors
    }
        #
    method getRegistry {} {
        return $DictRegistry
    }
        #
    method GetTabNames_All {} {
            # return a list with all tabnames
        set allNames {}
        dict for {w wDict} $DictRegistry {
            lappend allNames [dict get $wDict -text]
        }
        return $allNames
    }
    method GetTabIDs_All {} {
        return [dict keys $DictRegistry]
    }
    
    method GetTabIDs_Visible {} {
            # return a list with all active tabnames (active & selectabel):
            #     id -> name 
        set selectableTabIDs {}
        dict for {w wDict} $DictRegistry {
            if {[dict get $wDict -state] ne "hidden"} {
                lappend selectableTabIDs $w 
            }                
        }
        return $selectableTabIDs
    }
    method GetTabID_Name {name} {
            # search for tab with name: $name and return id
            puts "$DictRegistry"
        dict for {w wDict} $DictRegistry {
            puts "[dict get $wDict -text] eq $name"
            if {[dict get $wDict -text] eq $name} {
                return $w
            }
        }
        return {}
    }

    method SetTabID_Current {tabid} {
        dict for {w wDict} $DictRegistry {
            if {$w == $tabid} {
                set CurrentTabID $w
                dict set DictRegistry $w -state "active"
            } else {
                set status [dict get $wDict -state]
                if {$status ne "hidden"} {
                    dict set DictRegistry $w -state "normal"
                }
            }
        }
    }
    
    method UnifyTabID {tabid} {
        set ids [my GetTabIDs_All]
        if {[string is integer $tabid]} {
            set tab_int $tabid
            if {$tab_int >= 0} {
                set tabid [lindex [dict keys $DictRegistry] $tab_int]
            } else {
                set tabid {}
            }
        }
        if {[lsearch -exact $ids $tabid] >= 0} {
            return $tabid
        } else {
            return {}
        }
    }
    
    method CheckForCurrentTabID {{tabid {}}} {
            # puts "   ---> CheckForCurrentTab"
            #
        if {$tabid == {}} {
            set tabid $CurrentTabID
        }
            #
        set selectableTabIDs    [my GetTabIDs_Visible]
        set allTabIDs           [dict keys $DictRegistry]
            # puts "    ... GetTabIDs_Visible $selectableTabIDs <-?-> $allTabIDs"
            #
        if {[lsearch -exact $selectableTabIDs $tabid] < 0} {
                # puts "      ... define activeID"
            my SetTabID_Current [lindex $selectableTabIDs end]
                
        }
    }
    
    
    method SetError {id args} {
        switch -exact -- $id {
            E_001 {
                error "tabControl::Registry: Invalid slave specification $args"
            }
            default {
                puts "unknown ERROR: $id"
            }
        }
    }
        #
}