# ######################################################################
#
#  Copyright (c) Manfred ROSENBERGER, 2018
#
#      package: accordionMenu 	->	AccordionMenu.tcl
#
# ----------------------------------------------------------------------
#  namespace:  tabControl
# ----------------------------------------------------------------------
#

ttk::style layout Tabless.TNotebook.Tab {{}}

    # -----------------------------------------------------------------------------
    # class base declarations
    # -----------------------------------------------------------------------------
oo::class create tabControl::TabControl {
        #
    superclass tabControl::Observer
        #
    variable ObjectRootFrame
        #
    variable ObjRegistry
    variable ObjTabView
        #
    variable WidgetOptions
        #
    variable RemoteWidget
    variable TabsFrame    
        #
    variable RemoteWidgetTabs
        #
    variable TypeControl
    variable updateActive
        #
        # constructor {path {args {}}} {}
        #
    constructor {path args} {
            #
        set updateActive         0    ;# -> itemClickEvent -> disabled  
        set RemoteWidget        {}
        set RemoteWidgetTabs     1
            #
            # declaration of all additional widget options
        array set WidgetOptions $args
            #
        set args                [my ExtractArgument $args -typeControl  TypeControl]
        set args                [my ExtractArgument $args -offsetLeft   OffsetLeft]
        set args                [my ExtractArgument $args -paddingText  PaddingText]
            #
        if {$TypeControl == {}} {set TypeControl tab}
        if {$OffsetLeft == {}}  {set OffsetLeft 0}
            # puts "  ... \$TypeControl $TypeControl"
            # puts "  ... \$OffsetLeft  $OffsetLeft"
            #
            #
        set ObjectRootFrame     [frame ${path} -background lightblue -width 100 -height 30 -bd 0 -pady 0 -relief sunken]
            #
        set ObjRegistry         [tabControl::Registry new]
            #
        set ObjTabView          [tabControl::TabView new $ObjectRootFrame $TypeControl $OffsetLeft $PaddingText $ObjRegistry]
            #
            # my configure      {*}$args 
            #
            # ... subscribe to Registry
        $ObjRegistry subscribe  [self]
            #
        set updateActive           1    ;# -> itemClickEvent -> enabled
            #
    }

    destructor {
            # clean up once the widget get's destroyed
        set w [namespace tail [self]]
        catch {bind $w <Destroy> {}}
        catch {destroy $w}
            #
    }
    
    method unknown {method args} {
        return -code error "method \"$method\" is unknown"
    }

    method configure {args} {
            #
        if {[llength $args] == 0}  {
                #
                # as well as all custom options
            foreach xopt [array get WidgetOptions] {
                lappend opt_list $xopt
            }
            return $opt_list
                #
        } elseif {[llength $args] == 1}  {
                # return configuration value for this option
            set opt $args
            if { [info exists WidgetOptions($opt) ] } {
                return $WidgetOptions($opt)
            }
            return -code error "value for \"[lindex $args end]\" is not declared"
                #
        }
            #
            # error checking
        if {[expr {[llength $args]%2}] == 1}  {
            return -code error "value for \"[lindex $args end]\" missing"
        }
            #
            # process the new configuration options...
        array set opts $args
            #
        foreach opt_name [array names opts] {
                #
            set opt_value $opts($opt_name)
                # overwrite with new value
            if { [info exists WidgetOptions($opt_name)] } {
                set WidgetOptions($opt_name) $opt_value
            }
                #
        }
    }
    
        #
    method ExtractArgument {args keystr argName} {
            #
        upvar $argName  classArg ;#  reference to the variable to be set at class-level
            #
        set classArg ""
        while {[set i [lsearch -exact $args $keystr]] >= 0} {
            set j        [expr $i + 1]
            set classArg [lindex $args $j]
            set args     [lreplace $args $i $j] ;#  remove index $i and $j from args
        }
            #
        return $args
            #
    }

        #
        #
    method getPath      {} {}
        #
    method add          {window args} {}
    method configure    {args} {}
    method cget         {option} {}
    method forget       {tabid} {}
    method hide         {tabid} {}
    method identify     {type x y} {
                                # type: element, tab
                            }
    method index        {tabid} {}
    method insert       {pos subwindow args} {}
    method instate      {statespec {script {}}} {}
    method select       {{tabid {}}} {}
    method state        {{statespec {}}} {}
    method tab          {tabid args} {}
    method tabs         {} {} 
        #
}

    # -----------------------------------------------------------------------------
    # class method declarations
    # -----------------------------------------------------------------------------
oo::define tabControl::TabControl {

    method getPath {} {
        return $ObjectRootFrame 
    }
    
    method getRegistry {} {
        return [$ObjRegistry getRegistry]
    }
    method getCurrent_TabNeighbors {} {
        return [$ObjRegistry getCurrent_TabNeighbors]
    }
        #
    method occupyWidget {w} {
            #
            # puts "occupy $w"    
            # puts "[winfo class $w]"    
            #
        switch -exact [winfo class $w] {
            TNotebook {}
            default {
                puts "\n ... tabControl::TabControl is not compatible with <[winfo class $w]>\n"
                return
            }
        }
            #
        set RemoteWidget    $w
            #
        set tabActive       [$w select]
            #
        set remoteDict      {}
            #
        foreach tab [$w tabs] {
                #
                #
                # puts "[$w tab $tab]"
            set text        [$w tab $tab -text]
            set state       [$w tab $tab -state]
                # puts "         ... \$tab: $tab"
                # puts "             ... \$text:  $text"
                # puts "             ... \$state: $state"
                #
            dict set remoteDict $tab [list -text $text -state $state]
                #
        }
            #
            # puts "[dict print $remoteDict]"
            #
        $ObjRegistry setRegistry $remoteDict $tabActive
            #
            #
    }
    method getRemoteWidget {} {
        return $RemoteWidget
    }
    method updateFromRemoteWidget {} {
        puts "    ... t.b.d."
        if {$RemoteWidget != {}} {
            my occupyWidget $RemoteWidget
        }
    }
        #
    method updateState {args} {
            #
            # puts "    ... tabControl::TabControl -> updateState: -> $args"
            #
        set currentIndex    [$ObjRegistry getCurrent_TabID]
            # 
            # puts "    ... updateState: \$currentIndex -> $currentIndex"
            #
        if {$RemoteWidget == {}} {
            puts "\n    ... <W> tabControl::TabControl -> updateState: \$RemoteWidget ... not defined!\n"
            return
        }
            #
        switch -exact -- [lindex [join $args] 1] {
            addTabNew {
                    # puts "\n  ... add new Tab: handle with care!\n"
                set text    [$ObjRegistry getCurrent_Name]
                    # puts "$RemoteWidget add $currentIndex -text $text"
                    # puts "[winfo class $RemoteWidget]"
                $RemoteWidget add $currentIndex -text $text
                $RemoteWidget select $currentIndex
            }
            forgetTab {
                puts "\n  ... forget Tab: handle with care!\n        ... "
                puts "        ... forget Tab: handle with care!\n"
            }
            default {
                    # puts "\n  ... default: $args\n"
                $RemoteWidget select $currentIndex          
            }
        }

            # my ShowCurrentWindow
            #
    }
    
    method updateTabView {} {
        $ObjTabView updateState {force}
    }
        #
    method switchTabsState {{dict {}}} {
            #
        if {$dict == {}} {
            return [$ObjRegistry switchTabsState]
        }
            #
        $ObjRegistry switchTabsState $dict
            #
    }
    method switchRemoteControllerTabs {{state {}}} {
            #
            # puts "  -> switchRemoteControllerTabs -> $state <- init"
            #
        if {$state != {}} {
            if {$state == 0} {
                set RemoteWidgetTabs 1  ;# to be changed to "0" afterwords
            } else {
                set RemoteWidgetTabs 0
            }
        }
            # puts "  -> switchRemoteControllerTabs -> $RemoteWidgetTabs"
            #
            # puts "    a -> [winfo class $RemoteWidget]"
            # puts "    b -> [$RemoteWidget cget -class]"
            # puts "    c -> [$RemoteWidget cget -style]"
            # puts "    d -> [ttk::style layout TNotebook.Tab]"
            # puts "    e -> [ttk::style layout Tabless.TNotebook.Tab]"
        if {$RemoteWidgetTabs == 1} {
            ttk::style layout _Tabless_.TNotebook.Tab {{}}
            $RemoteWidget configure -style _Tabless_.TNotebook
            set RemoteWidgetTabs 0
        } else {
            $RemoteWidget configure -style {}
            set RemoteWidgetTabs 1
        }
            #
        if 1 {
                #
            set currentID           [my select]
            set currentNeighbors    [$ObjRegistry getCurrent_TabNeighbors]
                # puts "    f -> $currentID"
                # puts "    f -> $currentNeighbors"
            set tempID              [lindex [join $currentNeighbors] 0]
                # puts "    f -> $tempID"
            my select $tempID
                update
            my select $currentID
                #
        } else {
                # ... does not work
            update
            my updateState {force}
            my select [my select]
                #
        }
            #
            # puts "  -> switchRemoteControllerTabs -> $RemoteWidgetTabs <- done"
            #
    }
        #
    method add {window args} {
            #
            # puts "\n -----------------------\n"
            #
        set args    [my ExtractArgument $args -text tabText]
            #
        set allNames [$ObjRegistry getTabNames_All]
            # puts "                  -> \$allNames: $allNames <- $tabText"
        if 0 {
            if {[lsearch -exact -all $allNames $tabText] ne {}} {
                my SetError E_001
                return {}
            }
        }
            # ... add content to remoteWidget
        # pack $window -in $RemoteWidget -fill both -expand yes
        # pack configure $window -side right -anchor ne
            #
            # ... register content
        $ObjRegistry add $window $tabText
            #
        return ""
            #
    }
    method select {{tabid {}}} {
        if {$tabid == {}} {
            return [$ObjRegistry getCurrent_TabID]
        }
        $ObjRegistry select $tabid
    }
    method forget {tabid} {
        $ObjRegistry forget $tabid
    }
    method hide {tabid} {
        $ObjRegistry hide $tabid
    }
    method index {tabid} {
        return [$ObjRegistry index $tabid]
    }
    method tab {tabid args} {
        return [$ObjRegistry tab $tabid $args]
    }
    method tabs {} {
        return [$ObjRegistry tabs]
    }
        #
    method addByName {name} {
        $ObjRegistry addByName $name
    }
    method selectByName {name} {
        $ObjRegistry selectByName $name
    }
    method forgetByName {name} {
        $ObjRegistry forgetByName $name
    }
    method hideByName {name} {
        $ObjRegistry hideByName $name
    }


    # ---------------
    # private methods
    # ---------------
    
    method ShowCurrentWindow {} {
            #
            # puts "    ... ShowCurrentWindow:"
            # puts "        ... \$RemoteWidget: $RemoteWidget"
        set currentView     [$ObjRegistry getCurrent_TabID]  
            # puts "        ... \$currentView: $currentView"
            #
        if {$currentView == {}} return
            #
        pack $currentView -in $RemoteWidget -fill both -expand yes
            #
        foreach view [pack slaves $RemoteWidget] {
                # puts "               ... $view"
            if {$view != $currentView} {
                pack forget $view
            }
        }
    }

    method SetError {id} {
        switch -exact -- $id {
            E_001 {
                puts "  <E>\n  <E> tabControl::Registry: $id \n  <E>"
            }
            default {
                puts "  <E>\n  <E> unknown ERROR: $id \n  <E>"
            }
        }
    }


}
