 ##+##########################################################################
 #
 # package: tabControl -> Observer.tcl
 #
 #    Copyright (c) Manfred ROSENBERGER, 2022/06/25
 #
 # ---------------------------------------------------------------------------
 #    namespace:  tabControl - Observer
 # ---------------------------------------------------------------------------
 #

package require TclOO

oo::class create tabControl::Subject {
        #
    variable packageHomeDir
        #
    variable ObserverRegistry
    variable State
        #
    constructor {} {
            #
        puts "        -> class ViewModel"
            #
            #
        set packageHomeDir  $tabControl::packageHomeDir
            #
            #
        set ObserverRegistry    {}    
        set State               {}    
            #
    }
        #
    destructor { 
        puts "            [self] destroy"
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... tabControl::Subject  $target_method $args  ... unknown"
    }
        #
        #
    method subscribe {observer} {
            #
        if {![my CheckSubscription $observer]} {
            lappend ObserverRegistry $observer
        } else {
            puts "      subscription failed!"
        }
            #
        # puts "       ... \$ObserverRegistry $ObserverRegistry"
            #
    }
        #
    method unsubscribe {observer} {
        if {[my CheckSubscription $observer]} {
                # unset ObserverRegistry($observer)
            set idx [lsearch $ObserverRegistry $observer]
            set ObserverRegistry [lreplace $ObserverRegistry $idx $idx]
                # puts $ObserverRegistry
        }
    }
        #
    method notify {data} {
            #
        set State $data
            #
            # puts "\n"
            # puts "  --- tabControl -> notify: [self] ---------"
        foreach subscriber $ObserverRegistry {
            # puts "                  -> $subscriber"
            $subscriber updateState [self] $data
        }
            #
    }
        #
    method getData {} {
        return $State
    }
        #
    method CheckSubscription {observer} {
        if {$observer in $ObserverRegistry} {
            return 1
        } else {
            return 0
        }
    }
        #
    method reportSettings {} {
            #
        puts "\n"
        puts "        --- [self]: reportSettings ---------"
        puts ""
            #
        puts "            -> \$packageHomeDir       $packageHomeDir" 
        puts "            -> \$ObserverRegistry"
        foreach subscriber $ObserverRegistry {
            puts "                $subscriber"
        }
            #
        puts "\n"
            #
    }
        #
}

oo::class create tabControl::Observer {
        #
    variable packageHomeDir
    variable mySubject
        #
    constructor {subject} {
            #
        puts "        -> class Observer $subject"
            #
            #
        set packageHomeDir      $tabControl::packageHomeDir
            #
        set mySubject           $subject
        $mySubject subscribe    [self]
            #
    }
        #
    destructor { 
        puts "            [self] destroy"
        $mySubject unsubscribe [self]
    }
        #
    method unknown {target_method args} {
        puts "            <E> ... tabControl::Subject  $target_method $args  ... unknown"
    }
        #
    method updateState {subject args} {
        puts "            -> [self] -> updateState:"
        puts "                -> subject: $subject"
        set data [$subject getData]
        puts "                -> data: $data"
    }
        #
}

